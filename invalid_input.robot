*** Settings ***
Documentation     Data Driven


Library           FakerLibrary
Suite Setup    User Successfully Logged In
Test Setup    View Participants Table
Test Template     Invalid First Name And Last Name Should Not Be Submitted
Resource    resource.robot

*** Variable ***

${Long Word}    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout


*** Test Cases ***               FIRST NAME        LAST NAME

Symbol Input                     @sanchez          @sanchez
Number Input                     12345690          123809123
Long Word Input                  ${Long Word}      ${Long Word} 

*** Keywords ***
Invalid First Name And Last Name Should Not Be Submitted
    
    [Arguments]    ${fistname}    ${lastname}
    
    Input First Name    ${fistname}
    Input Last Name    ${lastname}
    Submit Participants Info
    Wait Until Element Is Visible    class:invalid-feedback
    Element Should Be Visible    class:invalid-feedback
    Set Selenium Implicit Wait    2



