*** Settings ***
Documentation     Example Gherkin Style Test

Library    Selenium2Library
Library    FakerLibrary

*** Variables ***

${LOGIN URL}    http://devapp.pfpd.com/login
${BROWSER}    Chrome
${HOME URL}    http://devapp.pfpd.com/home
${PARTICIPANTS URL}    http://devapp.pfpd.com/Participants


*** Keywords ***

User Successfully Logged In
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Input Text    email    admin@gmail.com
    Input Text    password    123456
    Click Button     Login
    Location Should Be   ${HOME URL} 
    Wait Until Page Contains    ICP AMS

View Participants Table
    Go To    ${PARTICIPANTS URL}
    Click Button    newParticipants

Input First Name
    [Arguments]    ${first_name}
    Input Text    first_name    ${first_name}

Input Last Name
    [Arguments]    ${last_name}
    Input Text    last_name    ${last_name}

Submit Participants Info
    Click Button    Create Participants