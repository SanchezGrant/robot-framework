*** Settings ***
Documentation     Example Gherkin Style Test


Library    FakerLibrary
Resource    resource.robot

*** Test Cases ***

Valid Registration
    Given User Successfully Logged In
    Then View Participants Table
    When User Register Valid Information
    Then Successful Message Should Appear

*** Keywords ***

User Register Valid Information

    ${first_name}    FakerLibrary.first_name
    ${last_name}    FakerLibrary.last_name
    
    ${remarks}    Word    nb=5
    ${get_count}    Get Element Count    tag:option
    ${count}    Evaluate    ${get_count} - 1

    ${Random Int}    Random Int    min=1    max=${get_count}
    ${chapel}    Convert To String    ${Random Int}


    Input Text    first_name    ${first_name}
    Input Text    last_name    ${last_name}
    Select From List By Index    chapel    ${chapel}
    Select Radio Button    registered    0
    Select Radio button    unpaid    0
    Input Text    remarks    ${remarks}
    Click Button    Create Participants

Successful Message Should Appear
     Wait Until Page Contains    Participants Created successfully
